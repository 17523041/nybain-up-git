<?= $this->extend('layout/template'); ?>

<?= $this->Section('content'); ?>
<div class="container">
	<div class="row">
		<div class="col">
			<h1>Kontak saya</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi autem laudantium, reprehenderit nobis, vitae esse, exercitationem voluptas non repudiandae magni, sequi culpa. Nobis, deserunt eum quidem officiis necessitatibus impedit, iusto.</p>
			<?php foreach ($alamat as $a) : ?>
				<ul>
					<li><?= $a['tipe'] ?></li>
					<li><?= $a['alamat'] ?></li>
					<li><?= $a['nomor'] ?></li>
				</ul>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?= $this->endSection(); 