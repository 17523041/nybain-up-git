<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
	<div class="row">
		<div class="col">
			<h1>Daftar Komik</h1>
			<a href="/komik/create" class="btn btn-primary">Tambah Komik</a>
			<a href="/komik/buat" class="btn btn-success">Buat Komik</a>
			<table class="table">
				<thead>
					<tr>
						<th scope="col">No</th>
						<th scope="col">Sampul</th>
						<th scope="col">Judul</th>
						<th scope="col">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($komik as $kom) : ?>
						<tr>

							<th scope="row"><?= $kom['id']; ?></th>
							<td><img src="/img/<?= $kom['sampul']; ?>" class="sampul"></td>
							<td><?= $kom['judul']; ?></td>
							<td>
								<a href="/komik/<?= $kom['slug']; ?>" class="btn btn-success">Detail</a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?= $this->endSection(); ?>