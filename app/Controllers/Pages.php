<?php namespace App\Controllers;

class Pages extends BaseController
{
	public function home()
	{
		$data = [
			'title' => 'Home | Halaman Aing',
			'tes' => ['satu', 'dua', 'tiga']
		];
		echo view('Pages/home', $data);

	}

	public function about()
	{
		$data = [
			'title' => 'About Aing | Halaman Aing'
		];
		echo view('Pages/about', $data);
	}

	public function contact() 
	{
		$data = [
			'title' => 'Contact Aing | Halaman Aing',
			'alamat' => [
				[
					'tipe' => 'rumah',
					'alamat' => 'jalan jalan ke padang mahsyar',
					'nomor' => '34C'
				],
				[
					'tipe' => 'kantor',
					'alamat' => 'jalan jalan ke gungung kidul',
					'nomor' => '20A'
				]
			]
		];
		echo view('Pages/contact', $data);
	}

	//--------------------------------------------------------------------

}
