<?php namespace App\Controllers;

use \App\Models\KomikModel;

class Komik extends BaseController
{
	protected $komikModel;

	public function __construct()
	{
		$this->komikModel = new KomikModel();
	}

	public function index()
	{
		// $komik = $this->komikModel->findAll();

		$data = [
			'title' => 'Komik | Halaman Aing',
			'judul' => 'Daftar Komik',
			'komik' => $this->komikModel->getKomik(),
		];

		// cara konek db tanpa model
		// $db = \Config\Database::connect();
		// $komik = $db->query("SELECT * FROM komik");
		// foreach ($komik->getResultArray() as $row) {
		// 	d($row);
		// }

		// return view('komik/index', $data);

		// $komikModel = new \App\Models\komikModel();


		return view('komik/index', $data);
	}

	public function detail($slug)
	{
		$data = [
			'title' => 'Detil Komik | Halaman Aing',
			'komik' => $this->komikModel->getKomik($slug)
		];

		return view('komik/detail', $data);
	}

	public function create()
	{
		$data = [
			'title' => 'Halaman Tambah'
		];

		return view('komik/create', $data);
	}

	public function buat()
	{
		$data = [
			'title' => 'Halaman buat'
		];

		return view('komik/buat', $data);
	}

	//--------------------------------------------------------------------

}
